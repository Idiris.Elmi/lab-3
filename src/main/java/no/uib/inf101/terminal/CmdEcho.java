package no.uib.inf101.terminal;

public class CmdEcho implements Command{

    @Override
    public String getName(){
        return "echo";
    }

    @Override
    public String run(String[] args) {
        String k = "";
        for (String arg : args) {
            k += arg + " ";
        }
        return k;
    }
}
